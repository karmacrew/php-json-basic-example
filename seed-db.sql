CREATE TABLE IF NOT EXISTS tasks (
 task_id MEDIUMINT NOT NULL AUTO_INCREMENT,
 title VARCHAR(100) NOT NULL,
 PRIMARY KEY (task_id)
);

INSERT INTO tasks (title) VALUES
('Mow the lawn'),
('Cut the grass'),
('Paint the fence'),
('Water the plants');
