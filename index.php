<?php

$link = mysqli_connect("127.0.0.1", "demojson", "demojson", "demojson");

if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

//Fetch all tasks
$result = mysqli_query($link, "SELECT * FROM tasks");;

$tasks = array();

while($row = $result->fetch_array(MYSQLI_ASSOC))
{
    $tasks[] = $row;
}

echo json_encode($tasks);
